const PATHS_DEV = {
  "styles": {
	  "src": "./assets/scss/**/*.scss",
	  "dest": "./assets/css/",
	  "dist": "./assets/dist/css"
  },
  "scripts": {
    "src": "./assets/js/**/*.js",
	  "dist": "./assets/dist/js"
  },
  "lang": {
    "src": "./assets/languages/**/*.po",
    "dest": "./languages/"
  }
}

exports.PATHS = {
  dev: PATHS_DEV
}
