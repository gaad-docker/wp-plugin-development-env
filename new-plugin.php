<?php
/**
 * Plugin Name:     New Plugin
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     new-plugin
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         New_Plugin
 */

// Your code starts here.
use App\Plugin\MainBootstrap;

const __NEW_PLUGIN__DIR = __DIR__;
const __NEW_PLUGIN_DIR__ = __NEW_PLUGIN__DIR . '/inc/Templates/';
define('__FABIOLA_CPA_CORE_URI__', explode("wp-content", get_stylesheet_directory_uri())[0] . "wp-content/plugins/" . basename(__NEW_PLUGIN__DIR));
if (file_exists(__NEW_PLUGIN__DIR . "/vendor/autoload.php")) require_once __NEW_PLUGIN__DIR . "/vendor/autoload.php";
include_once __NEW_PLUGIN__DIR . "/inc/class/MainBootstrap.php";

$mainBootstrap = MainBootstrap::getInstance();
