//extends support for ignore-commit-pattern for merge messages
module.exports = function (Handlebars) {
    Handlebars.registerHelper('custom', function (context, options) {
        let _options = context.data.root.options;
        let ignoreCommitPattern = undefined !== _options.ignoreCommitPattern ? _options.ignoreCommitPattern : false;
        let validReleases = []
        for (let i in context.data.root.releases){
            let release = context.data.root.releases[i];
            if(release.merges.length == 0 && release.commits.length == 0){
                continue;
            }
            if(release.tag == null){
               continue;
            }
            if (release.merges.length > 0) {
                let validMerges = []
                for (let j in release.merges){
                    let merge = release.merges[j];

                    if (undefined !== merge && ignoreCommitPattern && ! new RegExp(ignoreCommitPattern).test(merge.message)) {
                        validMerges.push(merge)
                        //console.log(merge.message);
                    }

                    release.merges = validMerges;
                }
            }
            validReleases.push(release)
        }
        context.data.root.releases = validReleases;
    })
}