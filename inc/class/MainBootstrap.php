<?php

namespace App\Plugin;

use App\Plugin\Core\BasicAssets;

class MainBootstrap
{
    private static ?MainBootstrap $instance = null;

    public static function getInstance(): MainBootstrap
    {
        if (static::$instance === null) {
            static::$instance = new self();
        }
        return static::$instance;
    }
    private function __construct()
    {
        new BasicAssets();
        $this->registerShortcodes();
        $this->registerEndpoints();
        $this->registerCPT();
        $this->registerCommands();
    }

    private function registerShortcodes(): void
    {
    }

    private function registerEndpoints(): void
    {
    }

    private function registerCPT(): void
    {
    }

    private function registerCommands(): void
    {
    }
}
