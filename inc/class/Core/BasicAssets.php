<?php

namespace App\Plugin\Core;

class BasicAssets
{
    public function __construct()
    {
    }

    public function enqueueAdminAssets()
    {
    }

    public function enqueueAssets()
    {
        $this->enqueueBoostrap();
    }

    public static function enqueueBoostrap(): void
    {
    }
}
