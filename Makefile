PHP_BIN ?= php
# Misc
.DEFAULT_GOAL = help
.PHONY        = help start

## —— 🎵 🐳 The GAAD-CLI Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

before-push: cbf cs phpstan unit-test

cs: ## Check code style with PHPCS
	 $(PHP_BIN) vendor/bin/phpcs -s --standard=PSR12 inc/class

cbf: ## Fix code style with PHPCBF
	 $(PHP_BIN) vendor/bin/phpcbf --standard=PSR12 inc/class

phpstan: ## Check code with PHPStan
	 $(PHP_BIN) vendor/bin/phpstan analyse inc/class

unit-test: ## Run unit testing
	 $(PHP_BIN) vendor/bin/phpunit -c phpunit.xml

unit-test-coverage: ## Run unit testing
	 $(PHP_BIN) -dxdebug.mode=coverage vendor/bin/phpunit -c phpunit.xml --coverage-html php-unit-coverage

unit-test-debug: ## Run unit testing with xdebug enabled
	 @PHP_INTERPRETER=$(PHP_BIN) PHP_IDE_CONFIG="serverName=gaad-cli" XDEBUG_CONFIG="idekey=PHPSTORM" $(PHP_BIN) -dxdebug.mode=debug -dxdebug.client_port=9003 -dxdebug.client_host=172.17.0.1 vendor/bin/phpunit -c phpunit.xml --testdox

gen-changelog:
	@if [ ! -d "node_modules" ]; then\
        npm i;\
    fi;\
	npm run changelog;
