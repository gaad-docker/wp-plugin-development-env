const fs = require('fs');
const gulp = require('gulp');
const gutil = require("gulp-util");
const env = require("gulp-env");
const sass = require('gulp-sass')(require('sass'));
const {PATHS} = require('./gulp-settings');
const {fail} = require("assert");
const minify = require('gulp-minify');
const poToMo = require('gulp-potomo-js');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

let ready = false;

function setEnv() {
	env({
		vars: {
			ENV: 'string' === typeof process.env.ENV ? process.env.ENV : 'dev'
		}
	});
}

function configTest() {
	let statusStack = [
		undefined !== PATHS[process.env.ENV]
	];

	let statusTest = statusStack.filter(function (elem, pos) {
		return statusStack.indexOf(elem) === pos;
	});

	let status = statusTest.length === 1 && statusTest[0]
	ready = status;

	gutil.log('Config test: ' + (status ? 'ok' : 'error'));
}

gulp.task('styles', function () {

	let partName = 'styles';
	let src = PATHS[process.env.ENV][partName].src;
	let dest = PATHS[process.env.ENV][partName].dest;
	if (!ready) {
		let msg = 'Gulp is not ready for env: ' + process.env.ENV + '. Please check the gulp-settings.js file and add . const PATHS_' + process.env.ENV.toUpperCase() + ' object.';

		return gulp.src('.')
			.pipe(fail(msg));
	}

	return gulp.src(src)
		.pipe(sass())
		.pipe(gulp.dest(dest))
});

gulp.task('minify-css', () => {
	let partName = 'styles';
	let dest = PATHS[process.env.ENV][partName].dist;
	return gulp.src(['./assets/css/*.css', './*.css'])
		.pipe(concat('plugin-stylesheet.css'))
		.pipe(cleanCss())
		.pipe(gulp.dest(dest));
});

gulp.task('minify-js', () => {
	let partName = 'scripts';
	let dist = PATHS[process.env.ENV][partName].dist;
	return gulp.src(['./assets/js/**/*.js'])
		.pipe(concat('plugin-scripts.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(dist));
});

gulp.task('lang', function () {

		let partName = 'lang';
		let src = PATHS[process.env.ENV][partName].src;
		let dest = PATHS[process.env.ENV][partName].dest;

		return gulp.src(src)
			.pipe(poToMo())
			.pipe(gulp.dest(dest))
	}
);

gulp.task('watch', () => {
	gulp.watch(PATHS[process.env.ENV]['styles'].src, (done) => {
		gulp.series(['styles', 'minify-css'])(done);
	});

	gulp.watch('./*.css', (done) => {
		gulp.series(['styles', 'minify-css'])(done);
	});

	gulp.watch(PATHS[process.env.ENV]['scripts'].src, (done) => {
		gulp.series(['minify-js'])(done);
	});

	gulp.watch(PATHS[process.env.ENV]['lang'].src, (done) => {
		gulp.series(['lang'])(done);
	});
});



setEnv();
configTest();
